Bonjour, 
Merci de nous avoir contacté! 

Votre demande a été enregistrée sous le numéro %{ISSUE_ID}.

Nous allons revenir vers vous dans les plus brefs délais. 

Bien cordialement,

L'équipe Go-Rénove et BDNB.

---
_Go-Rénove ne vous contactera jamais par téléphone, mail ou SMS pour vous proposer ses propres services de rénovation, vous demander un mot de passe ou un numéro de compte bancaire. Quand vous recevez un courriel, vérifiez toujours les liens proposés. En cas de doute, ne cliquez pas et contactez le support Go-Rénove via le [site Go-Rénove](https://gorenove.fr)._
