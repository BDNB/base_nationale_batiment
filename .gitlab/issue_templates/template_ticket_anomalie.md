## Résumé

(Résumé concis de l'anomalie)

## Comment reproduire et identifier l'anomalie

(copies d'écrans, identifiant d'objets, coordonnées géographique, description détaillée)

## Correctif attendu



/label ~anomalie
/cc @project-manager
