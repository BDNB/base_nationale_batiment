# Base Nationale des bâtiments

Projet public pour la publication, les discussions et anomalies autour de la base de données nationale des bâtiments diffusée en opendata

Pour relever le défi de la mise en place de la transition énergétique, le projet GORENOVE a permis le démarrage d'une base unifiée de l'ensemble des bâtiments de France. 

Ce projet préfigure la mise en oeuvre d'un véritable référentiel commun des batiments, portée par le projet Bat-ID.

Ce socle de données permet à tous les experts du batiment d'exploiter les données à l'échelle France entière

## Comprendre la BDNB

**Questions fréquentes, méthodologie de croisement complète, astuces d'utilisation** >> c'est sur le **[Wiki de la BDNB](https://gitlab.com/BDNB/base_nationale_batiment/-/wikis/home)** 

## Une anomalie, une idée d'amélioration, nous contacter

- Vous pouvez remonter des anomalies en [créant un ticket dans ce projet](https://gitlab.com/BDNB/base_nationale_batiment/-/issues/new?issuable_template=template_ticket_anomalie).  

- Une idée d'amélioration? une discussion plus large? vous pouvez utiliser [ce modèle de demande](https://gitlab.com/BDNB/base_nationale_batiment/-/issues/new?issuable_template=template_ticket_discussion)

- Vous êtes ayant droit aux fichiers fonciers ? Vous souhaitez accéder aux données de simulations produites par le CSTB? Vous avez besoin d'une API d'accès aux données spécifique pour votre projet? Utilisez [ce modèle de demande](https://gitlab.com/BDNB/base_nationale_batiment/-/issues/new?issuable_template=template_ticket_commercial&issue[confidential]=true&issue)

